import sys
import os

import numpy
import matplotlib
matplotlib.use('Agg')
from matplotlib import pyplot
from astropy.constants import G,L_sun,R_sun,M_sun
from amuse import datamodel
from amuse.datamodel import Particle, Particles
from amuse.units import units, constants
from amuse import datamodel
from amuse.community.seba.interface import SeBa
from amuse.community.mesa.interface import MESA
from amuse.community.huayno.interface import Huayno
from amuse.units.optparse import OptionParser

def mass_loss(RL, Rd):
    """Rate of mass loss from Roche lobe overflow.

    Assume convective envelope so gamma=5/3. Reduces exponent to 3.

    args:
        RL: Radius of Roche Lobe
        Rd: Radius of donor star

    returns:
        mass loss rate
    """

    return -((Rd-RL)/RL)**3.

def rochelobe(M1, M2, a):
    """Computes the Roche lobe radius of the system.

    args:
        M1: mass 1
        M2: mass 2
        a: semi-major axis

    returns:
        roche lobe radius

    """
    q = M1/M2
    ratio = (0.49 * numpy.power(q, 2./3.)) / (0.6*numpy.power(q, 2./3.) +
                                            numpy.log(1 + numpy.power(q, 1./3.)))
    return ((a * ratio)|units.RSun)

def calculate_semi_major_axis(Md, Ma, Rd):
    """Calculate semi-major axis.

    args:
        Md: donor mass
        Ma: accretor mass
        Rd: donor radius

    returns:
        semi-major axis

    """

    # Equation from Onno Pols: Binary
    q = (Md/ Ma)

    mu = G*(Md.value_in(units.kg) + Ma.value_in(units.kg))  # standard gravitational parameter of central body
    #print 'mu', mu
    P = (0.35 * Rd**(3./2.) * Md**(-1./2.) * (2/(1+q))**(0.2))*86400  #period in seconds (equation from Pols)
    P = P.number |units.s
    a = (((P/(2*numpy.pi))**2. * mu)**(1./3.)).number|units.m  #semi major axis in  meters

    return a.value_in(units.RSun)

def relative_velocity(total_mass, a):

    ##relative initial velocity between the members of the binary 

    return (constants.G * total_mass / a).sqrt()

def set_up_binary(a,Minit,Ma):

    ## initializing the positions, velocity and masses of the binary
    semimajor_axis = a
    eccentricity = 0.94
    masses = [Minit, Ma]
    orbital_period = (4 * numpy.pi**2 * a**3 /
        (constants.G * numpy.sum(masses))).sqrt().as_quantity_in(units.day)

    print "   Orbital period binary:", orbital_period
    binary =  Particles(2)
    binary.mass = masses
    binary.position = [0.0, 0.0, 0.0] | units.AU
    binary.velocity = [0.0, 0.0, 0.0] | units.km / units.s
    binary[0].x = semimajor_axis

    binary[0].vy = relative_velocity(binary.total_mass(),a)
    binary.move_to_center()
    print 'velocities of stars',binary[0].vy

    return binary




def make_RGB(Minit, z, rgb_time,binary):
    """Evolve a ZAMS star to a RGB.

    args:
        Minit: initial mass
        z: metallicity
        model_time: simulation model time

    returns:
        evolved RGB star from ZAMS

    """
    print 'evolving to RGB'
    stellar = SeBa()  #stellar evolution code

    stellar.parameters.metallicity = z
    stellar.particles.add_particles(binary)  #adding binary to stellar code

    model_time=0| units.Myr
    # Evolve the star
    dt = 1.0 | units.Myr   #time step
    while stellar.model_time< rgb_time:
        stellar.evolve_model(stellar.model_time + dt)
        model_time+=dt
        # Record information
    age = stellar.particles.age.in_(units.Myr)
    lum = stellar.particles.luminosity.in_(units.LSun)
    mass = stellar.particles.mass.in_(units.MSun)
    radius = stellar.particles.radius.in_(units.RSun)

    print 'RGB reached'
    print 'radius of rgb',radius
    print 'mass of rgb', mass
    return age, lum, mass, radius,stellar

def evolve_donor(Md, Ma, Rd, rgb_time, evolve_time,binary,stellar,a):
    """Evolve the RGB donor star.

    args:
        Md: initial RGB donor mass
        Ma: accretor mass
        Rd: initial donor radius
        evolve_time: simulation runtime

    returns:
        evolved donor star, donor radius, donor
        luminosity, and age.

    """

    Md_evolve = []
    Rd_evolve = []
    Ld_evolve = []
    age_evolve = []
    L_acc=[]
    model_time=rgb_time  #starts with the age of the rgb. so 1488 Myr
    dt = 0.01 | units.Myr  # time step for donor evolution
    sec_yr = 3.15e7 ##seconds in a year
    R_ns = 1e4 # neutron star radius in m
    while stellar.model_time < evolve_time:
        # Recalculate roche lobe variables
        # Assume conservation of mass
        Md_new = stellar.particles[0].mass.in_(units.MSun)
        Ma_new = Ma[0] + abs(Md - Md_new)[0]  ##adding lost mass from donor to accretor
        print 'mass donor',Md_new
        print 'mass accretor',Ma_new
        Rd_new = stellar.particles[0].radius.in_(units.RSun)

        a_new = calculate_semi_major_axis(Md_new, Ma_new, Rd_new)
        print 'radius star',a_new

        RL = rochelobe(Md_new, Ma_new, a.value_in(units.RSun))
        print 'roche lobe sausa', RL

        if Rd_new > RL:   # donor radius exceeds roche lobe radius
            print ('bigger!')

            # Set new mass loss
            dmdt = (mass_loss(RL, Rd) [0])|units.MSun/units.yr # mass loss rate 

            print 'mass loss',dmdt.number * dt.number
        
            # Evolve
            stellar.evolve_model(stellar.model_time + dt)

            M_core= stellar.particles[0].core_mass.number  ##using core mass from seba to estimate our analytical core mass
            Rd_l=3500 * (M_core)**4  #approximation for donor radius from Pols
            Md_evolve.append(Md_new.number-(dmdt.number*dt.number))  ##mass loss from exceeding Roche lobe    

            L_acc.append( (((G* Ma.value_in(units.kg)*dmdt.number*(M_sun/sec_yr)/R_ns).value)/L_sun).value) #3.
            Rd_new = Rd_l
            Rd_evolve.append(Rd_l)
            Ld_new = numpy.e**(3.50 + 8.11 * numpy.log(M_core/0.25) +0.61 *numpy.log(M_core/0.25)**2 + 2.13*numpy.log(M_core/0.25)**3)  ##constants from lecture 11 pols

            Ld_evolve.append(Ld_new)
            age_evolve.append(stellar.particles[0].age.in_(units.Myr).number)
            model_time+=dt
        else:  ## donor radius less than or equal to roche
            # Evolve
            stellar.evolve_model(stellar.model_time + dt)
            print ('smaller')
            # Save observables
            Md_evolve.append(stellar.particles[0].mass.in_(units.MSun).number)
            Rd_evolve.append(stellar.particles[0].radius.in_(units.RSun).number)
            Ld_evolve.append(stellar.particles[0].luminosity.in_(units.LSun).number)
            age_evolve.append(stellar.particles[0].age.in_(units.Myr).number)
            L_acc.append(0)
            model_time+=dt

    return Md_evolve, Rd_evolve, Ld_evolve, age_evolve,L_acc

def main(Minit,z,Ma,rgb_time,evolve_time,a):
    binary =  set_up_binary(a,Minit,Ma)

    vel = relative_velocity(Ma+Minit,a)

   
    age, Ld, Md, Rd,stellar = make_RGB(Minit, z, rgb_time,binary)
    

    Md_evolved, Rd_evolved, Ld_evolved, age_evolved,Lacc = \
            evolve_donor(Md, Ma, Rd,rgb_time, evolve_time,binary,stellar,a)
    print Md_evolved

    ##relevant plots

    #radius

    pyplot.plot(age_evolved, Rd_evolved)
    pyplot.xlabel('Age of RGB(Myr)')
    pyplot.ylabel('Radius ($R_\odot$)')
    pyplot.title('Change in radius')
    pyplot.xlim(numpy.min(age_evolved),numpy.max(age_evolved))
    pyplot.savefig('age_radius.png',dpi=500)
    pyplot.close()

    
    # Mass

    pyplot.plot(age_evolved, Md_evolved)
    pyplot.xlabel('Age of RGB(Myr)')
    pyplot.ylabel('Mass ($M_\odot$)')
    pyplot.title('Change in mass')
    pyplot.xlim(numpy.min(age_evolved),numpy.max(age_evolved))
    pyplot.savefig('age_mass.png',dpi=500)
    pyplot.close()

    ##luminosity accretor
    pyplot.plot(age_evolved, Lacc)
    pyplot.xlabel('Age of RGB(Myr)')
    pyplot.ylabel('Luminosity$_{acc}$ (L/$L_\odot$)')
    pyplot.title('Accretion Luminosity ')
    pyplot.xlim(numpy.min(age_evolved),numpy.max(age_evolved))
    pyplot.savefig('age_Lacc.png',dpi=500)
    pyplot.close()

     

    # Luminosity giant
    pyplot.semilogy(age_evolved, Ld_evolved)
    pyplot.xlabel('Age of RGB(Myr)')
    pyplot.ylabel('Luminosity ($L_\odot$)')
    pyplot.title('Change in luminosity')
    pyplot.xlim(numpy.min(age_evolved),numpy.max(age_evolved))
    pyplot.savefig('age_L.png',dpi=500)
    pyplot.close()


def new_option_parser():
    result = OptionParser()
    result.add_option("-M", unit=units.MSun,
                     dest="Minit", type="float",
                     default = 2,
                     help="Mass of donor star [%default]")

    result.add_option("-m", unit=units.MSun,
                     dest="Ma", type="float",
                     default = 1.4,
                     help="Mass of accretor star [%default]")

    result.add_option("-z", dest="z", type="float",
                      default = 0.02,
                      help="Metallicity [%default]")

    result.add_option("-a", unit=units.AU,
                  dest="a", type="float",
                  default = 2,
                  help="Semi-major axis [%default]")
    #
    # result.add_option("-e", dest="e", type="float",
    #                   default = 0.45,
    #                   help="Eccentricity [%default]")

    result.add_option("-t", unit=units.Myr,
                      dest="evolve_time", type="float",
                      default = 1495,
                      help="End time of the simulation [%default]")

    result.add_option("-T", unit=units.Myr,
                      dest="rgb_time", type="float",
                      default = 1488,
                      help="End time of the simulation [%default]")

    # result.add_option("-n", dest="n_steps", type="float",
    #                   default = 100,
    #                   help="Number of steps [%default]")
    
    return result
if __name__ == '__main__':
    o, args = new_option_parser().parse_args()
    sys.exit(main(**o.__dict__))
