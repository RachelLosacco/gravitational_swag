from amuse.lab import Particles, units
from amuse.units import nbody_system
import os
import subprocess
from matplotlib import animation
from matplotlib import pyplot
import numpy as np

# def animate(i):
    

def sun_venus_and_earth():
    '''3-body initial condition setup
    parameters include 3 body masses and initial velocities
    '''
    particles = Particles(3)
    sun = particles[0]
    sun.mass = 1.0 | nbody_system.mass
    sun.radius = 0.01 | nbody_system.length
    sun.position = (-1.0, 0.0, 0.0) | nbody_system.length
    sun.velocity = (0.6649107583, 0.832416784, 0.0) | (nbody_system.length/nbody_system.time)
    # sun.velocity = (0.3057224330, 0.5215124257, 0.0) | (nbody_system.length/nbody_system.time)
    # sun.velocity = (0.2009656237,0.2431076328, 0.0) | (nbody_system.length/nbody_system.time)
    venus = particles[1]
    venus.mass = 1.0 | nbody_system.mass
    venus.radius = 0.01 | nbody_system.length
    venus.position = (1.0, 0.0, 0.0) | nbody_system.length
    venus.velocity = (0.6649107583, 0.832416784, 0.0) | (nbody_system.length/nbody_system.time)
    # venus.velocity = (0.3057224330, 0.5215124257, 0.0) | (nbody_system.length/nbody_system.time)
    # venus.velocity = (0.2009656237,0.2431076328, 0.0) | (nbody_system.length/nbody_system.time)
    earth = particles[2]
    earth.mass = 2 | nbody_system.mass
    earth.radius = 0.01 | nbody_system.length
    earth.position = (0.0, 0.0, 0.0) | nbody_system.length
    earth.velocity = (-0.6649107583, -0.832416784, 0.0) | (nbody_system.length/nbody_system.time)
    # earth.velocity = (-0.3057224330, -0.5215124257, 0.0) | (nbody_system.length/nbody_system.time)
    # earth.velocity = (2*-0.2009656237/0.5,2*-0.2431076328/0.5, 0.0) | (nbody_system.length/nbody_system.time)
    particles.move_to_center()
    return particles

def integrate_solar_system(particles, end_time):
    """huayano integrator currently being used to solve gravitational interactions"""
    from amuse.lab import Huayno, nbody_system
   
    gravity = Huayno()
    gravity.particles.add_particles(particles)
    venus = gravity.particles[1]
    earth = gravity.particles[2]
    sun = gravity.particles[0]
    
    x_earth = [] | nbody_system.length 
    y_earth = [] | nbody_system.length 
    x_venus = [] | nbody_system.length
    y_venus = [] | nbody_system.length 
    x_sun = [] | nbody_system.length 
    y_sun = [] | nbody_system.length

    while gravity.model_time < end_time:
        gravity.evolve_model(gravity.model_time + (0.001 | nbody_system.time )) #units.day))
        x_earth.append(earth.x)
        y_earth.append(earth.y)
        x_venus.append(venus.x)
        y_venus.append(venus.y)
        x_sun.append(sun.x)
        y_sun.append(sun.y)
    gravity.stop()
    return x_earth, y_earth, x_venus, y_venus, x_sun, y_sun


def plot_track(xe,ye,xv,yv,xs,ys, output_filename):
    """plot the result of the 3 body problem """
    figure = pyplot.figure(figsize=(10, 10))
    pyplot.rcParams.update({'font.size': 30})
    # plot = figure.add_subplot(1,1,1)
    ax = pyplot.gca()
    ax.minorticks_on() 
    ax.locator_params(nbins=3)

    x_label = 'x '
    y_label = 'y '
    plot_title = 'I.A$_1^{i.c.}(2)$\n '
    pyplot.title(plot_title)
    pyplot.xlabel(x_label)
    pyplot.ylabel(y_label)
    
    fig, ax = pyplot.subplots(figsize=(5,5))
    files = []
    im_num = 0
    # pyplot.xlim(-1.5,1.5)
    # pyplot.ylim(-1.5,1.5)
    for i in range(0,12649,100): # 126 frames
        pyplot.cla()
        pyplot.xlim(-1.5,1.5)
        pyplot.ylim(-1.5,1.5)
        pyplot.scatter(xe.value_in(nbody_system.length)[i], ye.value_in(nbody_system.length)[i], color = 'b')
        pyplot.scatter(xv.value_in(nbody_system.length)[i], yv.value_in(nbody_system.length)[i], color = 'r')
        pyplot.scatter(xs.value_in(nbody_system.length)[i], ys.value_in(nbody_system.length)[i], color = 'g')
       #  pyplot.show()
        im_num += 1
        fname = '_tmp%i.png' % im_num
        print('saving frame...',fname)
        pyplot.savefig(fname)
        files.append(fname)
    subprocess.call("mencoder 'mf://_tmp*.png' -mf type=png:fps=10 -ovc lavc -lavacopts vcodec=wmv2 -oac copy -o animation.mpg",shell=True)
    # cleanup
    for fname in files:
        os.remove(files)
    # plot.set_xlim(-1.5, 1.5)
    # plot.set_ylim(-1.5, 1.5)

    # file = 'nbodyass1.png'
    # pyplot.savefig(file)
    # print '\nSaved figure in file', file,'\n'
    # pyplot.show()

def new_option_parser():
    """ handle input from user"""
    from amuse.units.optparse import OptionParser
    result = OptionParser()
    result.add_option("-o", 
                      dest="output_filename", default ="SunVenusEarth",
                      help="output filename [%default]") # set defaults if no inputs
    return result

if __name__ in ('__main__','__plot__'):
    o, arguments  = new_option_parser().parse_args()
    # T_1 = 19.013416290, T_2 = 8.8237067653
    particles = sun_venus_and_earth()
    xe,ye, xv,yv, xs,ys = integrate_solar_system(particles, 12.6489061509 | nbody_system.time)
    # print(np.size(xe)) -- 12649
    plot_track(xe, ye, xv, yv, xs, ys, o.output_filename)
    
