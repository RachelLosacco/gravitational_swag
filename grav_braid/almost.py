from amuse.lab import Particles, units
from amuse.units import nbody_system

def sun_venus_and_earth():
    particles = Particles(3)
    sun = particles[0]
    sun.mass = 1.0 | nbody_system.mass
    sun.radius = 0.01 | nbody_system.length
    sun.position = (-1.0, 0.0, 0.0) |nbody_system.length
    #sun.velocity = (0.3057224330, 0.5215124257, 0.0) | (nbody_system.length/nbody_system.time)
    sun.velocity = (0.2009656237,0.2431076328, 0.0) | (nbody_system.length/nbody_system.time)
    venus = particles[1]
    venus.mass = 1.0 | nbody_system.mass
    venus.radius = 0.01 | nbody_system.length
    venus.position = (1.0, 0.0, 0.0) | nbody_system.length
    #venus.velocity = (0.3057224330, 0.5215124257, 0.0) | (nbody_system.length/nbody_system.time)
    venus.velocity = (0.2009656237,0.2431076328, 0.0) | (nbody_system.length/nbody_system.time)
    earth = particles[2]
    earth.mass = 0.5 | nbody_system.mass
    earth.radius = 0.01 | nbody_system.length
    earth.position = (0.0, 0.0, 0.0) | nbody_system.length
    #earth.velocity = (-0.3057224330, -0.5215124257, 0.0) | (nbody_system.length/nbody_system.time)
    earth.velocity = (2*-0.2009656237/0.5,2*-0.2431076328/0.5, 0.0) | (nbody_system.length/nbody_system.time)
    particles.move_to_center()
    return particles

def integrate_solar_system(particles, end_time):
    from amuse.lab import Huayno, nbody_system
   

    gravity = Huayno()
    gravity.particles.add_particles(particles)
    venus = gravity.particles[1]
    earth = gravity.particles[2]
    sun = gravity.particles[0]
    
    x_earth = [] | nbody_system.length 
    y_earth = [] | nbody_system.length 
    x_venus = [] | nbody_system.length
    y_venus = [] | nbody_system.length 
    x_sun = [] | nbody_system.length 
    y_sun = [] | nbody_system.length

    while gravity.model_time < end_time:
        gravity.evolve_model(gravity.model_time + (0.001 | nbody_system.time )) #units.day))
        x_earth.append(earth.x)
        y_earth.append(earth.y)
        x_venus.append(venus.x)
        y_venus.append(venus.y)
        x_sun.append(sun.x)
        y_sun.append(sun.y)
    gravity.stop()
    return x_earth, y_earth, x_venus, y_venus, x_sun, y_sun


def plot_track(xe,ye,xv,yv,xs,ys, output_filename):

    from matplotlib import pyplot
    figure = pyplot.figure(figsize=(10, 10))
    pyplot.rcParams.update({'font.size': 30})
    plot = figure.add_subplot(1,1,1)
    ax = pyplot.gca()
    ax.minorticks_on() 
    ax.locator_params(nbins=3)

    x_label = 'x '
    y_label = 'y '
    pyplot.xlabel(x_label)
    pyplot.ylabel(y_label)

    plot.plot(xe.value_in(nbody_system.length), ye.value_in(nbody_system.length), color = 'b')
    plot.plot(xv.value_in(nbody_system.length), yv.value_in(nbody_system.length), color = 'r')
    plot.plot(xs.value_in(nbody_system.length), ys.value_in(nbody_system.length), color = 'g')
    plot.set_xlim(-1.5, 1.5)
    plot.set_ylim(-1.5, 1.5)

    file = 'sun_venus_earth_2.png'
    pyplot.savefig(file)
    print '\nSaved figure in file', file,'\n'
    pyplot.show()

def new_option_parser():
    from amuse.units.optparse import OptionParser
    result = OptionParser()
    result.add_option("-o", 
                      dest="output_filename", default ="SunVenusEarth",
                      help="output filename [%default]")
    return result
    

if __name__ in ('__main__','__plot__'):
    o, arguments  = new_option_parser().parse_args()

    particles = sun_venus_and_earth()
    xe,ye, xv,yv, xs,ys = integrate_solar_system(particles, 19.0134164290 | nbody_system.time) 
    plot_track(xe, ye, xv, yv, xs, ys, o.output_filename)
    
