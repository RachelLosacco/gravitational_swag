#! /bin/bash
#SBATCH -t 0:30:00
#SBATCH -N 1

module load AMUSE
module load openmpi

cp -r $HOME/run1 $TMPDIR

cd $TMPDIR/run1
srun mpiexec $AMUSE_HOME/amuse.sh tree_hybrid.py

mkdir -p $HOME/run1/results
cp * $HOME/run1/results
