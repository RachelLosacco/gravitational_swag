import numpy 

from amuse.lab import units
from amuse.lab import constants
from amuse.couple import bridge
from amuse.lab import nbody_system

#	=============================================================================

def dynamical_time_scale(M, R, G = constants.G):
	return numpy.sqrt(R**3/(G*M))

#	=============================================================================

def generate_cluster_particles(N, Mmin, Mmax, Mcut, Rvir, converter):
	
	from amuse.lab import new_plummer_model
	from amuse.lab import new_salpeter_mass_distribution
	
	print "Function: generate_cluster_particles"
	
	mass_function = new_salpeter_mass_distribution(N, Mmin, Mmax)
	Mtot = mass_function.sum()
	all_stars = new_plummer_model(N, convert_nbody = converter)
	all_stars.mass = mass_function
	all_stars.scale_to_standard(converter)		#revirialise cluster for IC
	
	scale = Mtot.value_in(units.MSun)
	Highpos =numpy.where(all_stars.mass > Mcut/scale)[0] 
	Lowpos =numpy.where(all_stars.mass < Mcut/scale)[0] 
	tdyn =dynamical_time_scale(Mtot, Rvir).in_(units.Myr)
	print "Cluster Dynamical Timescale = ", tdyn

	high_mass_stars = all_stars[Highpos]
	low_mass_stars =  all_stars[Lowpos]
	
	return high_mass_stars, low_mass_stars, tdyn

#	=============================================================================

def evolve_cluster(high_mass_stars,low_mass_stars, dt, t_end, converter, integrator,tdyn, filename):
	
	from amuse.ext.LagrangianRadii import LagrangianRadii
	import time
	
	
	print "Function: evolve_cluster"
	
	if (integrator == 1):
		from amuse.lab import Hermite
		gravity = Hermite(convert_nbody = converter)
		gravity.particles.add_particle(high_mass_stars)
		gravity.particles.add_particle(low_mass_stars)
	elif (integrator == 2):
		from amuse.lab import BHTree
		gravity = BHTree(convert_nbody = converter)
		gravity.particles.add_particle(high_mass_stars)
		gravity.particles.add_particle(low_mass_stars)
	elif (integrator == 3):

		from amuse.lab import ph4
		gravity_ph4 = ph4(convert_nbody = converter)
		
		from amuse.lab import BHTree
		gravity_BH = BHTree(convert_nbody = converter)	

		gravity_ph4.particles.add_particle(high_mass_stars)		#Setting the ph4 for High Mass Stars
		gravity_BH.particles.add_particle(low_mass_stars)			#Setting the Tree code for the Low Mass Stars

		gravity = bridge.Bridge(timestep= tdyn/50 ) 						#Building the bridge
		gravity.add_system(gravity_ph4, (gravity_BH,) )				#Creating the mutual influence of the N-body solvers
		gravity.add_system(gravity_BH, (gravity_ph4,) )
	
	diagnostics_time = []
	dE = []
	dE_rel = []
	R_50 = []
	R_Core = []
	
	t = 0 | t_end.unit	
	E_tot_init = gravity.kinetic_energy + gravity.potential_energy
	print "Cluster Initial Total Energy = ", E_tot_init
	RL = LagrangianRadii(gravity.particles)
	R_50_init = RL[6].in_(units.parsec)
	print "Cluster initial half-mass radius = ", R_50_init
	
	wall_time_start = time.time()
	
	while (t < t_end):
	
		dt = min(dt, t_end - t)
		print "Evolving for {}".format(dt)
		gravity.evolve_model(t + dt)		
		t += dt
		print "Evolved to {} out of {}".format(t, t_end)
		
		diagnostics_time.append(t)		
		E_tot = gravity.kinetic_energy + gravity.potential_energy			
		dE.append(E_tot - E_tot_init)
		dE_rel.append((E_tot - E_tot_init)/E_tot_init)		
		RL = LagrangianRadii(gravity.particles)
		R_50.append(RL[6].in_(units.parsec))		
		pos,coreradius,coredens = gravity.particles.densitycentre_coreradius_coredens(converter)
		R_Core.append(coreradius.in_(units.parsec))
	gravity.stop()
	
	wall_time_stop = time.time()
	
	#	Print and save diagnostics
	wall_time_elapsed = wall_time_stop - wall_time_start
	print "Integrating ", N, "bodies over ", t_end, "took ", wall_time_elapsed, "seconds."
	
	with open(filename, 'w') as file_handler:
		file_handler.write("Integrator: {}\n".format(integrator))
		file_handler.write("Amount of bodies: {}\n".format(N))
		file_handler.write("Cluster dynamical time: {}\n".format(tdyn))
		file_handler.write("Elapsed wall time: {}\n".format(wall_time_elapsed))
		file_handler.write("T, dE, dE_rel, R_50, R_Core\n")
		file_handler.write("{}, {}, , {}, {}\n".format(diagnostics_time[0].unit, 
																	dE[0].unit, 
																	R_50[0].unit, 
																	R_Core[0].unit))
		for i in range(0,len(dE)):
			file_handler.write("{}, {}, {}, {}, {}\n".format(diagnostics_time[i].number, 
																	numpy.abs(dE[i].number), 
																	numpy.abs(dE_rel[i]), 
																	R_50[i].number, 
																	R_Core[i].number))
	
#	=============================================================================

if __name__ in ("__main__"):
	
	#	Define the units we want to work in == define converter
	use_Msun_pc = nbody_system.nbody_to_si(1 | units.MSun, 1 | units.parsec)
	
	#	Global Cluster properties
	N = 1000																		#	Total number of stars
	Mmin = 0.1 | units.MSun											#	Minimum mass for Salpeter mass function
	Mmax = 100 | units.MSun										#	Maximum mass for Salpeter mass function
	Mcut = 1 |   units.MSun
	Rvir = 3 | units.parsec												#	Virial cluster radius
	
	#	Generate cluster
	high_mass_stars, low_mass_stars, tdyn = generate_cluster_particles(N, Mmin, Mmax, Mcut, Rvir, use_Msun_pc)
	
	#	Cluster evolution properties
	dt = .1 | units.Myr														#	Diagnostics timestep
	t_end = 10 | units.Myr												#	End time
	integrator = 1																#	Integration scheme: 1 == Nbody only, 
																						#	2 == Tree only, 3 == Hybrid
	out_filename = "diagnostics_pure_nbody.dat"	#	Diagnostics filename for output

	#	Evolve cluster
	evolve_cluster(high_mass_stars,low_mass_stars, dt, t_end, use_Msun_pc, integrator,tdyn, out_filename)
	
	print " \n\n\n\n\n REFERENCES"
