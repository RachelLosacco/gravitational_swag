#! /bin/bash
#SBATCH - 0:05:00
#SBATCH -N 1

module load AMUSE
module load openmpi

cp -r $HOME/run2 $TMPDIR

cd $TMPDIR/run2
srun mpiexec $AMUSE_HOME/amuse.sh n-body.py

mkdir -p $HOME/run2/results
cp * $HOME/run2/results
