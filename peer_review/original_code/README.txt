This directory contains the scripts used to work on assignment three.

The AMUSE script to generate results is contained in "n-body.py" and
"tree_hybrid.py". Only the choice of integrator differs for these codes.

Results of these scripts are outputted to the "diagnostics*.dat" files.

"plots.py" generates plots from these results.

A discussion of our results can be found in "report.pdf"

SWAG:
Our comments are expressed in the .py files, with the word "SWAG"
at the start. Our report is also in this directory titled
"peer_review_report.pdf".
