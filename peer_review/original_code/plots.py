# SWAG: don't shorten "pyplot" or "numpy" (RL)
from matplotlib import pyplot
import numpy
from collections import namedtuple
from amuse.lab import units

def Read_in(title):     #SWAG: Title of what? Assuming the input file
    data=open(title)
    splits = data.read().split('\n')
    names=splits[0].split(', ')

    T=numpy.array([]) | units.Myr
    dE=numpy.array([]) | units.m*2 * units.kg * units.s*-2
    dE_rel=numpy.array([])
    R_50=numpy.array([]) |units.parsec
    R_Core=numpy.array([]) |units.parsec
# SWAG: If this is the final draft of your script you should not include
# previously commented lines (RL)
    #Clock_Time =splits[4].split(', ')
    #CT = int(''.join(filter(str.isdigit, Clock_Time)))
    CT = float(splits[3].split(':')[1])
	#for k in range (len(names)):
	#	print (names[k], k)
	#	name=names[k]
	#	globals()[name]=numpy.array([])
    for i in range (6, len(splits)-6):			#the last two lines of data are the same
        lines=splits[i].split(', ')
        T=numpy.append(T, float(lines[0]))
        dE=numpy.append(dE, float(lines[1]))
        dE_rel=numpy.append(dE_rel, float(lines[2]))
        R_50=numpy.append(R_50, float(lines[3]))
        R_Core=numpy.append(R_Core, float(lines[4]))
    return (T, dE, dE_rel, R_50, R_Core,CT)

# SWAG: I'm not super sure what "split" does and there aren't a lot
# of comments explaining what's going on. I'm only assuming they're
# importing data from their .dat files in order to plot it (RL)

def Plot_1(sims,labs):      # SWAG: Not descriptive. Maybe "Plot_dE_rel():" (RL)
    for i in range(len(sims)):
    # SWAG: There's no indent! How much is being iterated??? (RL)
	T, dE, dE_rel, R_50, R_Core, CT = Read_in (sims[i])
	pyplot.plot(T, dE_rel,label = labs[i])
	pyplot.xlabel('T [Myr]')
	pyplot.ylabel('dE_rel')
	pyplot.title('Relative energy error over time')
    pyplot.legend()

    save_file = 'dE_rel_T.png'
    print '\nSaved figure in file', save_file,'\n'
    pyplot.savefig(save_file)
    pyplot.show()

def Plot_1x(sims,labs):     # SWAG: "Plot_dE_T():"
    for i in range(len(sims)):	# SWAG: Missing indents again
	T, dE, dE_rel, R_50, R_Core, CT = Read_in(sims[i])
	pyplot.plot(T, dE,label = labs[i])
	pyplot.xlabel('T [Myr]')
	pyplot.ylabel('dE')
	pyplot.title('Energy error over time')
    pyplot.legend()
    save_file = 'dE_T.png'
    print '\nSaved figure in file', save_file,'\n'
    pyplot.savefig(save_file)
    pyplot.show()

def Plot_2(sims,labs): # SWAG: Plot_half_radius():""
    for i in range(len(sims)):
        T, dE, dE_rel, R_50, R_Core, CT = Read_in(sims[i])
        pyplot.plot(T, R_50, label=labs[i])
        pyplot.xlabel('T')
        pyplot.ylabel('Radius [parsec]')
        pyplot.title('Half-mass radius over time')
# SWAG: I don't know if this is intended, but it's going to make a
# figure for every iteration. Only the final figure made will
# 1) have a legend, and 2) be saved. Probably not intended! (RL)
    pyplot.legend()
    save_file = 'R_50_T.png'
    print '\nSaved figure in file', save_file,'\n'
    pyplot.savefig(save_file)
    pyplot.show()

def Plot_2x(sims,labs):     # SWAG: Plot_core_radius():""
    for i in range(len(sims)): # SWAG: Indents!!
	T, dE, dE_rel, R_50, R_Core, CT = Read_in (sims[i])
	pyplot.plot(T, R_Core, label=labs[i])
	pyplot.xlabel('T')
	pyplot.ylabel('Radius [parsec]')
	pyplot.title('Core radius over time')

    pyplot.legend()
    save_file = 'Rc_T.png'
    print '\nSaved figure in file', save_file,'\n'
    pyplot.savefig(save_file)
    pyplot.show()

def Plot_3(T, M_cutoff):				#Need to give the cutof mass
    # SWAG: Plot_etc...
	pyplot.plot(T, M_cutoff)
	pyplot.xlabel('T')
	pyplot.ylabel('M_cutoff')
	pyplot.title('Relative energy error over time')
    # SWAG: Does this plot really show energy error over time?
    # You copy-pasted from the first plot function. Laaazzyyy! (RL)
    # SWAG: Well I guess they didn't care enough to be thorough, they
    # didn't even bother to save the file, just show it. (RL)
	pyplot.show()

def Plot_4():       # SWAG: Change name
    M_cut=0.5, 1.0, 2.5, 5.0, 25.0
    # SWAG: These numbers should not be assigned in here like this.
    # What if you run a different M_cut? It won't be included in this
    # script unless you go in an include it. You should have these numbers
    # taken from a source that automatically includes all M_cut values. (RL)
    dE_rel_end=[]
    for i in range (len(M_cut)):
        dE_rel=Read_in('diagnostics_Mcut_'+str(M_cut[i])+'.dat')[2]
        dE_rel_end.append(dE_rel[-1])

    pyplot.plot(M_cut, dE_rel_end)
    pyplot.xlabel('Cutoff mass in $M_{\odot}$')
    pyplot.ylabel('Relative energy error')
    pyplot.title('Relative energy error per cutoff mass')
    save_file = 'dE_rel_end_M_cutof.png'
    print '\nSaved figure in file', save_file,'\n'
    pyplot.savefig(save_file)
    pyplot.show()

def Plot_5():       # SWAG: Change name
    M_cut=0.5, 1.0, 2.5, 5.0,  25.0
    CT= numpy.zeros(len(M_cut))
    for i in range (len(M_cut)):
        CT[i]=Read_in('diagnostics_Mcut_'+str(M_cut[i])+'.dat')[5]
        CT[i]=numpy.round(CT[i]/60.)

    pyplot.plot(M_cut, CT)
    pyplot.xlabel('Cutoff mass in $M_{\odot}$')
    pyplot.ylabel('Wall-clock time in minutes')
    pyplot.title('Wall-clock time per cutoff mass')
    save_file = 'CT_M_cutof.png'
    print '\nSaved figure in file', save_file,'\n'
    pyplot.savefig(save_file)
    pyplot.show()

# SWAG: This is ugly (RL)
sims = ['diagnostics_Mcut_0.5.dat', 'diagnostics_Mcut_1.0.dat', 'diagnostics_Mcut_2.5.dat','diagnostics_Mcut_5.0.dat'
        , 'diagnostics_Mcut_25.0.dat', 'diagnostics_pure_nbody.dat','diagnostics_pure_tree.dat']
labs = ['Mc = 0.5','Mc = 1.0','Mc = 2.5','Mc = 5.0','Mc = 25','N-Body','Tree Code']

Plot_1(sims,labs)
Plot_1x(sims,labs)
Plot_2(sims,labs)
Plot_2x(sims,labs)
# SWAG: They didn't even run the third plotting script. Why did they keep it in?? (RL)
Plot_4()
Plot_5()

# SWAG: This script seems to have been done somewhat last minute. There were
# repeated mistakes, including odd spacing and indents. It's clearly copy-
# pasted, and the third plotting script is a mess they didn't bother to clean
# up. Personally I would simply include all of the plotting in the main script,
# though I can understand not using up supercomputer time simply doing plots.
# If this script is going to be a stand alone, it should rely more on an unknown
# number of inputs, rather than listing off file names and mass cut values verbatim.
# I would also like to see the plots saved in a separate directory so that the
# main directory is not cluttered with many .dat and .png files, but simply .py. (RL)
