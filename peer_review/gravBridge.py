import sys
import numpy
import time
import matplotlib
matplotlib.use("Agg")
import matplotlib.pyplot as plt

from amuse.lab import Hermite
from amuse.lab import BHTree
from amuse.lab import ph4
from amuse.lab import units
from amuse.lab import constants
from amuse.couple import bridge
from amuse.lab import nbody_system
from amuse.lab import new_plummer_model
from amuse.units.optparse import OptionParser
from amuse.lab import new_salpeter_mass_distribution
from amuse.ext.LagrangianRadii import LagrangianRadii

import plots

def dynamical_time_scale(M, R, G = constants.G):
    return numpy.sqrt(R**3/(G*M))

def generate_cluster_particles(N, Mmin, Mmax, Mcut, Rvir):
    """generate a plummer sphere of size N.

    From user inputted params, generates a standard-scaled plummer sphere with
    size N and various other physical properties.

    args:
        N: cluster size
        Mmin: minimum mass to be generated in the cluster
        Mmax: maximum mass
        Mcut: cutoff mass for two regimes
        Rvir: virial radius
        converter: units converter to standardize scale
        mass_function: generates mass distribution
        all_stars: plummer model

    returns:
        high_mass_stars: high mass stars in cluster from Mcut
        low_mass_stars: low mass stars in cluster
        tdyn: dynamical time scale
    """

    # Define the units we want to work in, solar mass units and parsecs
    converter = nbody_system.nbody_to_si(1 | units.MSun, 1 | units.parsec)

    print "Function: generate_cluster_particles"

    mass_function = new_salpeter_mass_distribution(N, Mmin, Mmax)
    Mtot = mass_function.sum()
    all_stars = new_plummer_model(N, convert_nbody = converter)
    all_stars.mass = mass_function
    all_stars.scale_to_standard(converter) # revirialise cluster for IC

    # scale = Mtot.value_in(units.MSun)
    # Highpos = numpy.where(all_stars.mass > Mcut/scale)[0]
    # Lowpos = numpy.where(all_stars.mass < Mcut/scale)[0]
    Highpos = numpy.where(all_stars.mass > Mcut)[0]
    Lowpos = numpy.where(all_stars.mass < Mcut)[0]
    tdyn = dynamical_time_scale(Mtot, Rvir).in_(units.Myr)
    print "Cluster Dynamical Timescale = ", tdyn

    high_mass_stars = all_stars[Highpos]
    low_mass_stars =  all_stars[Lowpos]

    return high_mass_stars, low_mass_stars, tdyn

def evolve_cluster(N,high_mass_stars,low_mass_stars, dt, t_end, \
                converter,integrator,tdyn,filename):
    """evolve cluster with selected gravity solving schemes.

    Based on user input, solves cluster dynamics using either the Hermite
    direct n-body code, BHTree code, or a bridged code using both BHTree and
    ph4 direct n-body solvers.

    args:
        diagnostics_time: age of cluster
        dE: change in energy from initial total energy
        dE_rel = relative energy error per timestep
        R_50 = half-mass radius found using LagrangianRadii in AMUSE
        R_Core = core radius found using LagrangianRadii in AMUSE

    returns:
        none. Passes args into write_to_file for output
    """

    print "Function: evolve_cluster"

    if (integrator == 1):
        gravity = Hermite(convert_nbody = converter)
        gravity.particles.add_particle(high_mass_stars)
        gravity.particles.add_particle(low_mass_stars)
    elif (integrator == 2):
        gravity = BHTree(convert_nbody = converter)
        gravity.particles.add_particle(high_mass_stars)
        gravity.particles.add_particle(low_mass_stars)
    elif (integrator == 3):
        gravity_ph4 = ph4(convert_nbody = converter)
        gravity_BH = BHTree(convert_nbody = converter)

        #Setting the ph4 for High Mass Stars
        #Setting the Tree code for the Low Mass Stars
        gravity_ph4.particles.add_particle(high_mass_stars)
        gravity_BH.particles.add_particle(low_mass_stars)

        #Building the bridge
        gravity = bridge.Bridge(timestep = tdyn/50)

        #Creating the mutual influence of the N-body solvers
        gravity.add_system(gravity_ph4,(gravity_BH,))
        gravity.add_system(gravity_BH,(gravity_ph4,))

    diagnostics_time = []
    dE = []
    dE_rel = []
    R_50 = []
    R_Core = []

    t = 0 | t_end.unit
    E_tot_init = gravity.kinetic_energy + gravity.potential_energy
    print "Cluster Initial Total Energy = ", E_tot_init
    RL = LagrangianRadii(gravity.particles)
    R_50_init = RL[6].in_(units.parsec)
    print "Cluster initial half-mass radius = ", R_50_init

    # start timer for beginning of simulation
    wall_time_start = time.time()

    while (t < t_end):
        dt = min(dt, t_end - t)
        print "Evolving for {}".format(dt)
        gravity.evolve_model(t + dt)
        t += dt
        print "Evolved to {} out of {}".format(t, t_end)

        diagnostics_time.append(t)
        E_tot = gravity.kinetic_energy + gravity.potential_energy
        dE.append(E_tot - E_tot_init)
        dE_rel.append((E_tot - E_tot_init)/E_tot_init)
        RL = LagrangianRadii(gravity.particles)
        R_50.append(RL[6].in_(units.parsec))
        pos,coreradius,coredens = \
            gravity.particles.densitycentre_coreradius_coredens(converter)
        R_Core.append(coreradius.in_(units.parsec))
    gravity.stop()

    wall_time_stop = time.time()
    wall_time_elapsed = wall_time_stop - wall_time_start
    print("Integrating ", N, "bodies over ", t_end, "took ", \
                                                wall_time_elapsed, "seconds.")
    write_to_file(filename,integrator,N,tdyn,wall_time_elapsed, \
                    diagnostics_time,dE,R_50,R_Core,dE_rel)

def write_to_file(filename,integrator,N,tdyn,wall_time_elapsed, \
                diagnostics_time,dE,R_50,R_Core,dE_rel):
    """Print and save diagnostics from simulation.

    args:
        filename: outfile filename
        integrator: gravity scheme chosen
        N: cluster size
        tdyn: dynamical time scale of cluster
        wall_time_elapsed: runtime of program
        diagnostics_time: age of cluster
        dE: change in total energy from initial
        dE_rel: relative energy error
        R_50: half-mass radius
        R_Core: core radius

    returns:
        data file
    """

    with open(filename, 'w') as file_handler:
        file_handler.write("Integrator: {}\n".format(integrator))
        file_handler.write("Amount of bodies: {}\n".format(N))
        file_handler.write("Cluster dynamical time: {}\n".format(tdyn))
        file_handler.write("Elapsed wall time: {}\n".format(wall_time_elapsed))
        file_handler.write("T, dE, dE_rel, R_50, R_Core\n")
        file_handler.write("{}, {}, , {}, {}\n".format(
                                                    diagnostics_time[0].unit,
                                                    dE[0].unit,
                                                    R_50[0].unit,
                                                    R_Core[0].unit))
        for i in range(0,len(dE)):
            file_handler.write("{}, {}, {}, {}, {}\n".format(
                                                diagnostics_time[i].number,
                                                numpy.abs(dE[i].number),
                                                numpy.abs(dE_rel[i]),
                                                R_50[i].number,
                                                R_Core[i].number))

def main(Nstars,smin,smax,cut,r,timestep,end,rseed,fname,scheme):
    # set random seed
    seed = rseed
    numpy.random.seed(seed)

    # Define the units we want to work in, solar mass units and parsecs
    use_Msun_pc = nbody_system.nbody_to_si(1 | units.MSun, 1 | units.parsec)

    # Global cluster properties: number of stars in cluster N, the minimum mass
    # found in the cluster Mmin, maximum mass Mmax, the cutoff point between
    # the two regimes Mcut, and the virial radius Rvir
    N = Nstars
    Mmin = smin
    Mmax = smax
    Mcut = cut
    Rvir = r

    # Generate cluster
    high_mass_stars, low_mass_stars, tdyn = generate_cluster_particles(
        N, Mmin, Mmax, Mcut, Rvir, use_Msun_pc)

    # Cluster evolution properties
    # out_filename includes cluster properties and simulation properties
    # described in write_to_file().
    dt = timestep
    t_end = end
    integrator = scheme
    out_filename = fname

    # Evolve cluster
    evolve_cluster(N,high_mass_stars,low_mass_stars, dt, t_end,
                    use_Msun_pc,integrator,tdyn,out_filename)
    
    plots.main()

    print " \n\n\n\n\n REFERENCES"


def new_option_parser():
    """initializes a suite of valid user-inputs.

    args:
        scheme, N (number of stars in cluster), Mcut, max (maximum stellar
        mass), min (minimum stellar mass), virial radius, timestep,
        evolution time, output file name)

    returns:
        inputs as a list
     """

    input = OptionParser()
    input.add_option("-s",
                      dest="scheme", type="int", default = 2,
                      help="gravity code scheme to be used [%default]")

    input.add_option("-n",
                      dest="Nstars", type="int", default = 10000,
                      help="number of stars in cluster [%default]")

    input.add_option("--mc", unit=units.MSun,
                      dest="cut", type="float",default = 10 | units.MSun,
                      help="solar mass cutoff point [%default]")

    input.add_option("--min", unit=units.MSun,
                      dest="smin", type="float",default = 0.1 | units.MSun,
                      help="minimum mass in cluster distribution [%default]")

    input.add_option("--max", unit=units.MSun,
                      dest="smax", type="float",default = 100. | units.MSun,
                      help="maximum mass in cluster distribution [%default]")

    input.add_option("--r", unit=units.parsec,
                      dest="r", type="int",default = 3 | units.parsec,
                      help="virial radius [%default]")

    input.add_option("--dt", dest="timestep", type="float",
                      default = 0.1 | units.Myr,
                      help="time evolution resolution (timestep) [%default]")

    input.add_option("-t", unit=units.Myr,
                      dest="end", type="float", default = 10 | units.Myr,
                      help="end time of the simulation [%default]")

    input.add_option("--rand",
                      dest="rseed", type="int", default = 0,
                      help="random seed [%default]")

    input.add_option("--f",
                      dest="fname", type="str", default = 'output.txt',
                      help="data output filename [%default]")

    return input

if __name__ in ("__main__"):
    o, args  = new_option_parser().parse_args()
    sys.exit(main(**o.__dict__))
