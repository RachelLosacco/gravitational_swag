"""nosetesting for gravBridge.py"""

from __future__ import division
import numpy
from nose.tools import *

from amuse.lab import Particles, nbody_system, constants
from amuse.units import units

import gravBridge

def test_n_generate_cluster_particles():
    # test if number of particles of properly sized
    hm,lm,tdyn = gravBridge.generate_cluster_particles(10000,
                                                        0.1|units.MSun,
                                                        100|units.MSun,
                                                        1|units.MSun,
                                                        3|units.parsec)
    assert (len(hm)+len(lm)) == 1e4

@raises(NotImplementedError) # decorator for String input type
def test_generatre_cluster_particles_string():
    hm,lm,tdyn = gravBridge.generate_cluster_particles('',
                                                        0.1|units.MSun,
                                                        100|units.MSun,
                                                        1|units.MSun,
                                                        3|units.parsec)

@raises(NotImplementedError) # decorator for None input type
def test_generatre_cluster_particles_none():
    hm,lm,tdyn = gravBridge.generate_cluster_particles(None,
                                                        0.1|units.MSun,
                                                        100|units.MSun,
                                                        1|units.MSun,
                                                        3|units.parsec)

def main():
    test_n_generate_cluster_particles()
    test_generatre_cluster_particles_string()
    test_generatre_cluster_particles_none()

if __name__ == ("__main__"):
    sys.exit(main())
