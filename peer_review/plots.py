import os
import glob as glob
import matplotlib
matplotlib.use("Agg")
from matplotlib import pyplot as plt
import numpy as np
from collections import namedtuple

from amuse.lab import units

def Read_in(title):
    """split relevant data from each simulation data file.

    data files are read in and organized into a readable format

    args:
        T: age (from 0 to 10Myr in 0.1 Myr increments)
        dE: total energy difference at each timestep vs initial E
        dE_rel: relative energy error at each timestep
        R_50: half-mass radius at each step
        R_Core: core radius at each step
        CT: runtime per simulation

    returns:
        args in list format
    """

    # open and organize data
    data=open(title)
    splits = data.read().split('\n')
    names=splits[0].split(', ')

    T=np.array([]) | units.Myr
    dE=np.array([]) | units.m*2 * units.kg * units.s*-2
    dE_rel=np.array([])
    R_50=np.array([]) |units.parsec
    R_Core=np.array([]) |units.parsec
    CT = float(splits[3].split(':')[1])

    # last two lines contain same info
    # populate data arrays
    for i in range (6, len(splits)-6):
        lines=splits[i].split(', ')
        T=np.append(T, float(lines[0]))
        dE=np.append(dE, float(lines[1]))
        dE_rel=np.append(dE_rel, float(lines[2]))
        R_50=np.append(R_50, float(lines[3]))
        R_Core=np.append(R_Core, float(lines[4]))
    return (T, dE, dE_rel, R_50, R_Core,CT)



def plot_relE_err_over_time(sims,labs):
    # Relative energy error over time
    for i in range(len(sims)):
        T, dE, dE_rel, R_50, R_Core, CT = Read_in (sims[i])
        plt.plot(T, dE_rel,label = labs[i])
        
    plt.xlabel('T [Myr]')
    plt.ylabel('dE_rel')
    plt.title('Relative energy error over time')
    plt.legend()

    save_file = 'dE_rel_T.png'
    print '\nSaved figure in file', save_file,'\n'
    plt.savefig(save_file)
    plt.show()

def plot_totE_err_over_time(sims,labs):
    # Energy difference over time
    for i in range(len(sims)):
	    T, dE, dE_rel, R_50, R_Core, CT = Read_in (sims[i])
	    plt.plot(T, dE,label = labs[i])

	plt.xlabel('T [Myr]')
	plt.ylabel('dE')
	plt.title('Energy difference over time')
    plt.legend()
    save_file = 'dE_T.png'
    print '\nSaved figure in file', save_file,'\n'
    plt.savefig(save_file)
    plt.show()

def plot_half_mass_rad_over_time(sims,labs):
    # Half-mass radius over time
    for i in range(len(sims)):
        T, dE, dE_rel, R_50, R_Core, CT = Read_in (sims[i])
        plt.plot(T, R_50, label=labs[i])

    plt.xlabel('T')
    plt.ylabel('Radius [parsec]')
    plt.title('Half-mass radius over time')
    plt.legend()
    save_file = 'R_50_T.png'
    print '\nSaved figure in file', save_file,'\n'
    plt.savefig(save_file)
    plt.show()

def plot_core_rad_over_time(sims,labs):
    # Core radius over time
    for i in range(len(sims)):
	    T, dE, dE_rel, R_50, R_Core, CT = Read_in (sims[i])
	    plt.plot(T, R_Core, label=labs[i])

    plt.xlabel('T')
    plt.ylabel('Radius [parsec]')
    plt.title('Core radius over time')
    plt.legend()
    save_file = 'Rc_T.png'
    print '\nSaved figure in file', save_file,'\n'
    plt.savefig(save_file)
    plt.show()

def plot_relE_err_vs_mcut(labs):
    # Relative energy error vs cutoff mass
    dE_rel_end=[]
    for i in range (len(M_cut)):
        dE_rel=Read_in(labs[i])[2]
        dE_rel_end.append(dE_rel[-1])

    plt.plot(M_cut, dE_rel_end)
    plt.xlabel('Cutoff mass in $M_{\odot}$')
    plt.ylabel('Relative energy error')
    plt.title('Relative energy error per cutoff mass')
    save_file = 'dE_rel_end_M_cutof.png'
    print '\nSaved figure in file', save_file,'\n'
    plt.savefig(save_file)
    plt.show()

def plot_runtime_vs_mcut(labs):
    # Wall-clock time vs cutoff mass
    CT = np.zeros(len(M_cut))
    for i in range (len(M_cut)):
        CT[i] = Read_in(labs[i])[5]
        CT[i] = np.round(CT[i]/60.)

    plt.plot(M_cut, CT)
    plt.xlabel('Cutoff mass in $M_{\odot}$')
    plt.ylabel('Wall-clock time in minutes')
    plt.title('Wall-clock time per cutoff mass')
    save_file = 'CT_M_cutof.png'
    print '\nSaved figure in file', save_file,'\n'
    plt.savefig(save_file)
    plt.show()

def main():
    sims = glob.glob('*.dat')
    labs = [os.path.basename(x) for x in sims]
    plot_relE_err_over_time(sims,labs)
    plot_totE_err_over_time(sims,labs)
    plot_half_mass_rad_over_time(sims,labs)
    plot_core_rad_over_time(sims,labs)
    plot_relE_err_vs_mcut(labs)
    plot_runtime_vs_mcut(labs)

if __name__ == ('__main__'):
    main()
