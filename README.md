###### COMPUTATIONAL ASTROPHYSICS 2019 ######

To Sander,

The organization is as follows:

/gravitational_swag is the name of our computational astrophysics repository

assignments are in their own folders from there, as

Assignment 1: grav_braid (gravitational braids)

Assignment 2: triplet_evolve (theta-muscae assignment)

Assignment 3: grav_bridge (gravity_bridge treecode and direct n-body)

Assignment 4: peer_review (files from peer group assignment 3 with our edits and comments)

Final: lmxb (low-mass X-ray binary)

All assignment folders contain their own readme.

###### COMPUTATIONAL ASTROPHYSICS 2019 ######
